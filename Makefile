KERNEL_TREE := /lib/modules/$(shell uname -r)/build
# KERNEL_TREE := $(HOME)/linux-$(KERN_VERSION)

PWD := $(shell pwd)

#EXTRA_CFLAGS += -O2 -DCONFIG_DM_DEBUG -fno-inline -Wall -fno-omit-frame-pointer -g
EXTRA_CFLAGS += -O2 -DCONFIG_DM_DEBUG -fno-inline -Wall 
# EXTRA_CFLAGS += -O2 -UCONFIG_DM_DEBUG

obj-m := dm-nvm.o
dm-nvm-objs := \
	lightnvm.o \
	reg.o \
	core.o \
	gc.o


all:
	$(MAKE) -C $(KERNEL_TREE) M=$(PWD) modules

clean:
	$(MAKE) -C $(KERNEL_TREE) M=$(PWD) clean
